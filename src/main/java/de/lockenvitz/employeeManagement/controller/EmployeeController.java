package de.lockenvitz.employeeManagement.controller;

import de.lockenvitz.employeeManagement.model.Employee;
import de.lockenvitz.employeeManagement.service.EmployeeServiceInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/employee")
public class EmployeeController {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);
    private final EmployeeServiceInterface employeeService;

    public EmployeeController(final EmployeeServiceInterface employeeService) {
        this.employeeService = employeeService;
    }

    //todo: Improvement: Bad Request if salutation not valid

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> createEmployee(@Valid @RequestBody final Employee employee) {
        return ResponseEntity.ok(employeeService.save(employee));
    }

    @GetMapping(value = "/id/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> getEmployee(@PathVariable final Long id) {
        final Optional<Employee> employee = employeeService.findById(id);
        if (employee.isEmpty()) {
            logger.error("Employee with ID: " + id + " not exists");
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(employee.get());
    }

    @GetMapping(value = "/employeeid/{employeeId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> getEmployeeByEmployeeId(@PathVariable final String employeeId) {
        final Optional<Employee> employee = employeeService.findByEmployeeId(employeeId);
        if (employee.isEmpty()) {
            logger.error("Employee with ID: " + employeeId + " not exists");
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(employee.get());
    }

    @PatchMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> updateEmployee(
            @PathVariable final Long id,
            @Valid @RequestBody final Employee newEmployee) {
        final Optional<Employee> existingEmployee = employeeService.findById(id);
        if (existingEmployee.isEmpty()) {
            logger.error("Employee with ID: " + id + " not exists");
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(employeeService.save(existingEmployee.get(), newEmployee));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable final Long id) {
        if (employeeService.findById(id).isEmpty()) {
            logger.error("Employee with ID: " + id + " not exists");
            return ResponseEntity.badRequest().build();

        } else {
            employeeService.deleteById(id);
            return ResponseEntity.ok().build();
        }
    }
}
