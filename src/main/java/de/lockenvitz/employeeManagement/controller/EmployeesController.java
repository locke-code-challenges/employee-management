package de.lockenvitz.employeeManagement.controller;

import de.lockenvitz.employeeManagement.model.Employee;
import de.lockenvitz.employeeManagement.service.EmployeeServiceInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/employees")
public class EmployeesController {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);
    private final EmployeeServiceInterface employeeService;

    public EmployeesController(final EmployeeServiceInterface employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Employee>> getAllEmployees() {
        final List<Employee> employeeList = employeeService.findAll();
        if (employeeList.isEmpty()) {
            logger.info("No employees found");
        }

        return ResponseEntity.ok(employeeList);
    }

    @GetMapping(value = "/salary", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Employee>> getAllEmployeesWithSalary(
            @RequestParam(name = "value") final double value,
            @RequestParam(name = "alignment", defaultValue = "exact") final String alignment) {
        final List<Employee> employeeList = employeeService.findBySalary(value, alignment);
        if (employeeList.isEmpty()) {
            logger.info("No employees with given: " + value + " found");
        }

        return ResponseEntity.ok(employeeList);
    }

    @GetMapping(value = "/salutation/men", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Employee>> getAllMaleEmployees() {
        final List<Employee> employeeList = employeeService.findByMenSalutation();
        if (employeeList.isEmpty()) {
            logger.info("No male employees found");
        }

        return ResponseEntity.ok(employeeList);
    }

    @GetMapping(value = "/lastname/{lastName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Employee>> getAllEmployeesWithLastName(@PathVariable final String lastName) {
        final List<Employee> employeeList = employeeService.findByLastName(lastName);
        if (employeeList.isEmpty()) {
            logger.info("No employees with given lastName: " + lastName + "found");
        }

        return ResponseEntity.ok(employeeList);
    }
}
