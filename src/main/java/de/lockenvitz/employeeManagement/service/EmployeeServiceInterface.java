package de.lockenvitz.employeeManagement.service;

import de.lockenvitz.employeeManagement.model.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeServiceInterface {

    //todo: Improvement: Interface to restrictive. It should be an abstraction of an abstract EmployeeService

    Optional<Employee> findById(final Long id);

    Optional<Employee> findByEmployeeId(final String employeeId);

    Employee save(final Employee employee);

    Employee save(final Employee employee, final Employee newEmployee);

    void deleteById(final Long id);

    List<Employee> findAll();

    List<Employee> findBySalary(final double salary, final String alignment);

    List<Employee> findByMenSalutation();

    List<Employee> findByLastName(final String lastName);
}
