package de.lockenvitz.employeeManagement.service;

import de.lockenvitz.employeeManagement.model.Employee;
import de.lockenvitz.employeeManagement.model.Salutation;
import de.lockenvitz.employeeManagement.respository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class EmployeeService implements EmployeeServiceInterface {

    private static final Salutation HERR = Salutation.HERR;
    private final EmployeeRepository employeeRepository;

    public EmployeeService(final EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Optional<Employee> findById(final Long id) {
        return employeeRepository.findById(id);
    }

    @Override
    public Optional<Employee> findByEmployeeId(final String employeeId) {
        return employeeRepository.findFirstByEmployeeId(employeeId);
    }

    @Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    @Override
    public List<Employee> findBySalary(final double salary, final String alignment) {

        List<Employee> employeeList;

        switch (alignment) {
            case "above":
                employeeList = employeeRepository.findBySalaryGreaterThan(salary);
                break;
            case "below":
                employeeList = employeeRepository.findBySalaryLessThan(salary);
                break;
            default:
                employeeList = employeeRepository.findBySalaryEquals(salary);
        }

        return employeeList;
    }

    @Override
    public List<Employee> findByMenSalutation() {
        return employeeRepository.findBySalutationEquals(HERR);
    }

    @Override
    public List<Employee> findByLastName(final String lastName) {
        return employeeRepository.findByLastNameEquals(lastName);
    }

    @Override
    public Employee save(final Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public Employee save(final Employee employee, final Employee newEmployee) {
        employee.setEmployeeId(newEmployee.getEmployeeId());
        employee.setLastName(newEmployee.getLastName());
        employee.setSalary(newEmployee.getSalary());
        employee.setSalutation(newEmployee.getSalutation());
        employee.setSureName(newEmployee.getSureName());

        return save(employee);
    }

    @Override
    public void deleteById(final Long id) {
        employeeRepository.deleteById(id);
    }
}
