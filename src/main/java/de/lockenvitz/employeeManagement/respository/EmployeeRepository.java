package de.lockenvitz.employeeManagement.respository;

import de.lockenvitz.employeeManagement.model.Employee;
import de.lockenvitz.employeeManagement.model.Salutation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    //todo: Improvement: just a workaround to avoid Internal Server Error. employee_id should be unique.
    // Avoid to adding employee_id more than once.

    @Query(value = "SELECT * FROM employee e WHERE e.employee_id = ?1 LIMIT 1", nativeQuery = true)
    Optional<Employee> findFirstByEmployeeId(final String employeeId);

    List<Employee> findBySalaryEquals(final double salary);

    //todo: Exist a better solution instead of using a native sql query?
    @Query(value = "SELECT * FROM employee e WHERE e.salary < ?1", nativeQuery = true)
    List<Employee> findBySalaryLessThan(final double salary);

    //todo: Exist a better solution instead of using a native sql query?
    @Query(value = "SELECT * FROM employee e WHERE e.salary > ?1", nativeQuery = true)
    List<Employee> findBySalaryGreaterThan(final double salary);

    List<Employee> findBySalutationEquals(final Salutation salutation);

    List<Employee> findByLastNameEquals(final String lastName);

    List<Employee> findAll();
}
