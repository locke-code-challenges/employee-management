package de.lockenvitz.employeeManagement.model;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "employee")
public class Employee {

    @Id
    @GeneratedValue
    private Long id;

    //todo: Improvement: employee_id should be unique
    @Column(name = "employee_id")
    @NotNull(message = "value cannot be null")
    @NotBlank(message = "value cannot be blank")
    private String employeeId;

    @Column(name = "salutation")
    @NotNull(message = "value cannot be null")
    @Enumerated(EnumType.STRING)
    private Salutation salutation;

    @Column(name = "sure_name")
    @NotNull(message = "value cannot be null")
    @NotBlank(message = "value cannot be blank")
    private String sureName;

    @Column(name = "last_name")
    @NotNull(message = "value cannot be null")
    @NotBlank(message = "value cannot be blank")
    private String lastName;

    @Column(name = "salary", precision=16, scale=2)
    @NotNull(message = "value cannot be null")
    @DecimalMin("0.00")
    private double salary;

    public Employee(
            final Long id,
            final String employeeId,
            final Salutation salutation,
            final String sureName,
            final String lastName,
            final double salary) {
        this.id = id;
        this.employeeId = employeeId;
        this.salutation = salutation;
        this.sureName = sureName;
        this.lastName = lastName;
        this.salary = salary;
    }

    public Employee() {
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(final String employeeId) {
        this.employeeId = employeeId;
    }

    public Salutation getSalutation() {
        return salutation;
    }

    public void setSalutation(final Salutation salutation) {
        this.salutation = salutation;
    }

    public String getSureName() {
        return sureName;
    }

    public void setSureName(final String sureName) {
        this.sureName = sureName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(final double salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Double.compare(employee.salary, salary) == 0 &&
                Objects.equals(id, employee.id) &&
                Objects.equals(employeeId, employee.employeeId) &&
                salutation == employee.salutation &&
                Objects.equals(sureName, employee.sureName) &&
                Objects.equals(lastName, employee.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, employeeId, salutation, sureName, lastName, salary);
    }
}
