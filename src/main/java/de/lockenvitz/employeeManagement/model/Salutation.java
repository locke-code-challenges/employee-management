package de.lockenvitz.employeeManagement.model;

public enum Salutation {
    HERR,
    FRAU,
    DIVERS
}
