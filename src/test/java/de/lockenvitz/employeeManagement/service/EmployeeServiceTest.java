package de.lockenvitz.employeeManagement.service;

import de.lockenvitz.employeeManagement.base.BaseTest;
import de.lockenvitz.employeeManagement.model.Employee;
import de.lockenvitz.employeeManagement.respository.EmployeeRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class EmployeeServiceTest extends BaseTest {

    //todo: Improvement: Add test where salutation not valid

    @InjectMocks
    private EmployeeService classUnderTest;

    @Mock(name = "EmployeeRepository")
    private EmployeeRepository employeeRepository;

    @Test
    @DisplayName("findById should return employee")
    void findById_should_return_employee() {
        //todo: not much logic here for an explicit test
    }

    @Test
    @DisplayName("findByEmployeeId should return employee")
    void findByEmployeeId_should_return_employee() {
        //todo: not much logic here for an explicit test
    }

    @Test
    @DisplayName("findAll should return a list of employees")
    void findAll_should_return_a_list_of_employees() {
        //todo: not much logic here for an explicit test
    }

    @Test
    @DisplayName("findBySalary should return a list of employees")
    void findBySalary_should_return_a_list_of_employees() {
        //todo: not much logic here for an explicit test
    }

    @Test
    @DisplayName("findByMenSalutation should return a list of employees")
    void findByMenSalutation_should_return_a_list_of_employees() {
        //todo: not much logic here for an explicit test
    }

    @Test
    @DisplayName("findByLastName should return a list of employees")
    void findByLastName_should_return_a_list_of_employees() {
        //todo: not much logic here for an explicit test
    }

    @Test
    @DisplayName("save should save and return an employee")
    void save_should_save_and_return_an_employee() {
        when(employeeRepository.save(EMPLOYEE)).thenReturn(EMPLOYEE_5);

        Employee result = classUnderTest.save(EMPLOYEE, EMPLOYEE_5);

        verify(employeeRepository, times(1)).save(EMPLOYEE);
        assertThat(result, is(EMPLOYEE_5));
    }

    @Test
    @DisplayName("deleteById should delete an employee")
    void deleteById_should_delete_an_employee() {
        //todo: not much logic here for an explicit test
    }
}