package de.lockenvitz.employeeManagement.base;

import de.lockenvitz.employeeManagement.model.Employee;
import de.lockenvitz.employeeManagement.model.Salutation;
import org.springframework.http.HttpHeaders;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

public class BaseTest {

    protected static final long ID = 1L;
    protected static final String EMPLOYEE_ID = "employee1";
    protected static final Employee EMPLOYEE = new Employee(
            1L,
            "employee1",
            Salutation.HERR,
            "sureName",
            "lastName",
            52000.99);
    protected static final Employee EMPLOYEE_2 = new Employee(
            1L,
            "employee1",
            Salutation.HERR,
            "sureName",
            "lastName",
            54000.99);
    protected static final Employee EMPLOYEE_3 = new Employee(
            2L,
            "employee2",
            Salutation.DIVERS,
            "sureName",
            "lastName",
            60000.99);
    protected static final Employee EMPLOYEE_4 = new Employee(
            2L,
            "employee2",
            Salutation.DIVERS,
            "sureName",
            "lastName",
            60000.99);
    protected static final Employee EMPLOYEE_5 = new Employee(
            3L,
            "employee3",
            Salutation.FRAU,
            "otherSureName",
            "otherLastName",
            62000.0);

    protected static final List<Employee> EXPECTED_EMPLOYEE_LIST = List.of(EMPLOYEE_2, EMPLOYEE_3);

    protected static final String HTTP_LOCALHOST = "http://localhost:";
    protected static final String API_PATH = "/api";
    protected static final String API_VERSION = "/v1";

    protected URI getUri(final String endpoint, final int port) throws URISyntaxException {
        return new URI(HTTP_LOCALHOST + port + API_PATH + API_VERSION + "/" + endpoint);
    }

    protected HttpHeaders getHttpHeader(final Map<String, String> headers) {
        final HttpHeaders httpHeaders = new HttpHeaders();
        headers.forEach(httpHeaders::set);

        return httpHeaders;
    }
}
