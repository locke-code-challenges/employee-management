package de.lockenvitz.employeeManagement.controller;

import de.lockenvitz.employeeManagement.base.BaseTest;
import de.lockenvitz.employeeManagement.model.Employee;
import de.lockenvitz.employeeManagement.service.EmployeeServiceInterface;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class EmployeesControllerTest extends BaseTest {

    @InjectMocks
    private EmployeesController classUnderTest;

    @Mock
    private EmployeeServiceInterface employeeService;

    @Test
    @DisplayName("getAllEmployees should return a list of employee")
    void getAllEmployees_should_return_a_list_of_employee() {
        when(employeeService.findAll()).thenReturn(EXPECTED_EMPLOYEE_LIST);

        ResponseEntity<List<Employee>> result = classUnderTest.getAllEmployees();

        assertThat(result.getStatusCodeValue(), is(200));
        assertThat(result.getBody(), is(EXPECTED_EMPLOYEE_LIST));
        verify(employeeService, times(1)).findAll();
    }

    @Test
    @DisplayName("getAllEmployees should return empty list if no employee exists")
    void getAllEmployees_should_return_empty_list_if_no_employee_exists() {
        when(employeeService.findAll()).thenReturn(Collections.emptyList());

        ResponseEntity<List<Employee>> result = classUnderTest.getAllEmployees();

        assertThat(result.getStatusCodeValue(), is(200));
        assertThat(result.getBody(), is(Collections.emptyList()));
        verify(employeeService, times(1)).findAll();
    }

    @Test
    @DisplayName("getAllEmployeesWithSalary should return a list of employee")
    void getAllEmployeesWithSalary_should_return_a_list_of_employee() {
        final double salary = 60000.99;
        final String alignment = "";

        when(employeeService.findBySalary(salary, alignment)).thenReturn(List.of(EMPLOYEE_2));

        ResponseEntity<List<Employee>> result = classUnderTest.getAllEmployeesWithSalary(salary, alignment);

        assertThat(result.getStatusCodeValue(), is(200));
        assertThat(result.getBody(), is(List.of(EMPLOYEE_2)));
        verify(employeeService, times(1)).findBySalary(salary, alignment);
    }

    @Test
    @DisplayName("getAllEmployeesWithSalary should return empty list if no employee with given salary exists")
    void getAllEmployeesWithSalary_should_return_empty_list_if_no_employee_with_given_salary_exists() {
        final double salary = 54000.01;
        final String alignment = "";

        when(employeeService.findBySalary(salary, alignment)).thenReturn(Collections.emptyList());

        ResponseEntity<List<Employee>> result = classUnderTest.getAllEmployeesWithSalary(salary, alignment);

        assertThat(result.getStatusCodeValue(), is(200));
        assertThat(result.getBody(), is(Collections.emptyList()));
        verify(employeeService, times(1)).findBySalary(salary, alignment);
    }

    @Test
    @DisplayName("getAllMaleEmployees should return a list of employee")
    void getAllMaleEmployees_should_return_a_list_of_employee() {
        //todo: same procedure like the other tests with result ok
    }

    @Test
    @DisplayName("getAllMaleEmployees should return empty list if no male employee exists")
    void getAllMaleEmployees_should_return_empty_list_if_no_male_employee_exists() {
        //todo: same procedure like the other tests with bad result
    }

    @Test
    @DisplayName("getAllEmployeesWithLastName should return a list of employee")
    void getAllEmployeesWithLastName_should_return_a_list_of_employee() {
        //todo: same procedure like the other tests with result ok
    }

    @Test
    @DisplayName("getAllEmployeesWithLastName should return empty list if no employee with given last name exists")
    void getAllEmployeesWithLastName_should_return_empty_list_if_no_employee_with_given_last_name_exists() {
        //todo: same procedure like the other tests with bad result
    }
}