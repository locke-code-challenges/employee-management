package de.lockenvitz.employeeManagement.controller;

import de.lockenvitz.employeeManagement.base.BaseTest;
import de.lockenvitz.employeeManagement.model.Employee;
import de.lockenvitz.employeeManagement.model.Salutation;
import de.lockenvitz.employeeManagement.respository.EmployeeRepository;
import de.lockenvitz.employeeManagement.service.EmployeeService;
import de.lockenvitz.employeeManagement.service.EmployeeServiceInterface;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EmployeeControllerComponentTest extends BaseTest {

    private static final String EMPLOYEE_ENDPOINT = "employee";

    @LocalServerPort
    private int RANDOM_SERVICE_PORT;

    private EmployeeController classUnderTest;
    private EmployeeServiceInterface employeeService;
    private EmployeeRepository employeeRepository;
    private TestRestTemplate testRestTemplate;

    @BeforeEach
    public void setUp() {
        testRestTemplate = new TestRestTemplate();
        employeeRepository = mock(EmployeeRepository.class);
        employeeService = new EmployeeService(employeeRepository);
        classUnderTest = new EmployeeController(employeeService);
    }

    @Test
    @DisplayName("updateEmployee should update employee successfully")
    void updateEmployee_should_update_employee_successfully() {
        when(employeeRepository.findById(ID)).thenReturn(Optional.of(EMPLOYEE));
        when(employeeRepository.save(EMPLOYEE)).thenReturn(EMPLOYEE_5);

        ResponseEntity<Employee> result = classUnderTest.updateEmployee(ID, EMPLOYEE_5);

        assertThat(result.getStatusCodeValue(), is(200));
        assertThat(result.getBody(), is(EMPLOYEE_5));
    }

// todo: the following kind of test to test the UPDATE endpoint is much better way than the test above. The problem is,
//  in this case the rest template needs the HttpMethod.PATCH methods, which are currently not full supported. It needs
//  a especially workaround to do so, but it isn't high priority for me at the moment.
//
//    @Test
//    @DisplayName("updateEmployee should update employee successfully")
//    void updateEmployee_should_update_employee_successfully() throws URISyntaxException, MalformedURLException {
//        final Employee employee = new Employee(3L, "employee3", Salutation.FRAU, "otherSureName", "otherLastName", 62000.0);
//        final URI uri = getUri(EMPLOYEE_ENDPOINT, RANDOM_SERVICE_PORT);
//        final HttpHeaders header = getHttpHeader(Map.of("Content-Type", "application/json"));
//        final HttpEntity<Employee> request = new HttpEntity<>(employee, header);
//
//        when(employeeRepository.findById(ID)).thenReturn(Optional.of(EMPLOYEE));
//        when(employeeRepository.save(EMPLOYEE)).thenReturn(EMPLOYEE_5);
//
//        //todo: Patch won't work here. See: https://github.com/spring-cloud/spring-cloud-netflix/issues/1777
//        ResponseEntity<Employee> result = testRestTemplate.exchange(uri.toURL() + "/1L", HttpMethod.PATCH, request, Employee.class);
//
//        verify(employeeRepository, times(1)).findById(ID);
//        verify(employeeRepository, times(1)).save(employee);
//        assertThat(result.getStatusCodeValue(), is(200));
//        assertThat(result.getBody(), is(EMPLOYEE_5));
//    }

    @Nested
    @DisplayName("createEmployee should create no new employee because request body is not valid")
    class CreateEmployeeShouldCreateNoNewEmployee {

        @Test
        @DisplayName("because attributes are null")
        void attributes_are_null() throws URISyntaxException {
            final Employee employee = new Employee(1L, null, null, null, null, 0.00);

            final ResponseEntity<String> result = getEmployeeSalaryResponse(employee);

            assertThat(result.getStatusCodeValue(), is(400));
        }

        @Test
        @DisplayName("because attributes are blank")
        void attributes_are_blank() throws URISyntaxException {
            final Employee employee = new Employee(1L, "", Salutation.HERR, "", "", 0.00);

            final ResponseEntity<String> result = getEmployeeSalaryResponse(employee);

            assertThat(result.getStatusCodeValue(), is(400));
        }

        @Test
        @DisplayName("because salary has no valid value")
        void salary_is_under_allowed_minimum_value() throws URISyntaxException {
            final Employee employee = new Employee(1L, "employee1", Salutation.HERR, "sureName", "lastName", -1.00);

            final ResponseEntity<String> result = getEmployeeSalaryResponse(employee);

            assertThat(result.getStatusCodeValue(), is(400));
        }

        private ResponseEntity<String> getEmployeeSalaryResponse(final Employee employee) throws URISyntaxException {
            final URI uri = getUri(EMPLOYEE_ENDPOINT, RANDOM_SERVICE_PORT);
            final HttpHeaders header = getHttpHeader(Map.of("Content-Type", "application/json"));
            final HttpEntity<Employee> request = new HttpEntity<>(employee, header);

            return testRestTemplate.postForEntity(uri, request, String.class);
        }
    }
}