package de.lockenvitz.employeeManagement.controller;

import de.lockenvitz.employeeManagement.base.BaseTest;
import de.lockenvitz.employeeManagement.model.Employee;
import de.lockenvitz.employeeManagement.respository.EmployeeRepository;
import de.lockenvitz.employeeManagement.service.EmployeeService;
import de.lockenvitz.employeeManagement.service.EmployeeServiceInterface;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EmployeesControllerComponentTest extends BaseTest {

    private static final String SALARY_ENDPOINT = "employees/salary";

    @LocalServerPort
    private int RANDOM_SERVICE_PORT;

    @MockBean
    private EmployeeRepository employeeRepository;

    private TestRestTemplate testRestTemplate;

    @BeforeEach
    public void setUp() {
        testRestTemplate = new TestRestTemplate();
    }

    @Nested
    @DisplayName("getAllEmployeesWithSalary should return a list of employees")
    class GetAllEmployeesWithSalary {

        @Test
        @DisplayName("with all employees with default salary alignment")
        void all_employees_default_salary_alignment() throws MalformedURLException, URISyntaxException {
            final double salary = 60000.99;
            final String url = "?value=" + salary;

            when(employeeRepository.findBySalaryEquals(salary)).thenReturn(List.of(EMPLOYEE_3, EMPLOYEE_4));

            final ResponseEntity<List<Employee>> result = getEmployeesSalaryResponse(url);

            verify(employeeRepository, times(1)).findBySalaryEquals(salary);
            assertThat(result.getStatusCodeValue(), is(200));
            assertThat(result.getBody(), is(List.of(EMPLOYEE_3, EMPLOYEE_4)));
        }


        @Test
        @DisplayName("with all employees with full specified salary")
        void all_employees_full_specified_salary() throws MalformedURLException, URISyntaxException {
            final double salary = 54000.99;
            final String alignment = "exact";
            final String url = "?value=" + salary + "&alignment=" + alignment;

            when(employeeRepository.findBySalaryEquals(salary)).thenReturn(List.of(EMPLOYEE_2));

            final ResponseEntity<List<Employee>> result = getEmployeesSalaryResponse(url);

            verify(employeeRepository, times(1)).findBySalaryEquals(salary);
            assertThat(result.getStatusCodeValue(), is(200));
            assertThat(result.getBody(), is(List.of(EMPLOYEE_2)));
        }

        @Test
        @DisplayName("with empty list because no employees found with full specified salary")
        void empty_list_full_specified_salary() throws MalformedURLException, URISyntaxException {
            final double salary = 59999.99;
            final String alignment = "exact";
            final String url = "?value=" + salary + "&alignment=" + alignment;

            when(employeeRepository.findBySalaryEquals(salary)).thenReturn(Collections.emptyList());

            final ResponseEntity<List<Employee>> result = getEmployeesSalaryResponse(url);

            verify(employeeRepository, times(1)).findBySalaryEquals(salary);
            assertThat(result.getStatusCodeValue(), is(200));
            assertThat(result.getBody(), is(Collections.emptyList()));
        }

        @Test
        @DisplayName("with all employees with below specified salary")
        void all_employees_below_specified_salary() throws MalformedURLException, URISyntaxException {
            final double salary = 60000.00;
            final String alignment = "below";
            final String url = "?value=" + salary + "&alignment=" + alignment;

            when(employeeRepository.findBySalaryLessThan(salary)).thenReturn(List.of(EMPLOYEE, EMPLOYEE_2));

            final ResponseEntity<List<Employee>> result = getEmployeesSalaryResponse(url);

            verify(employeeRepository, times(1)).findBySalaryLessThan(salary);
            assertThat(result.getStatusCodeValue(), is(200));
            assertThat(result.getBody(), is(List.of(EMPLOYEE, EMPLOYEE_2)));
        }

        @Test
        @DisplayName("with empty list because no employees found with below specified salary")
        void empty_list_below_specified_salary() throws MalformedURLException, URISyntaxException {
            final double salary = 59999.99;
            final String alignment = "below";
            final String url = "?value=" + salary + "&alignment=" + alignment;

            when(employeeRepository.findBySalaryLessThan(salary)).thenReturn(Collections.emptyList());

            final ResponseEntity<List<Employee>> result = getEmployeesSalaryResponse(url);

            verify(employeeRepository, times(1)).findBySalaryLessThan(salary);
            assertThat(result.getStatusCodeValue(), is(200));
            assertThat(result.getBody(), is(Collections.emptyList()));
        }

        @Test
        @DisplayName("with all employees with above specified salary")
        void all_employees_above_specified_salary() throws MalformedURLException, URISyntaxException {
            final double salary = 60000.00;
            final String alignment = "above";
            final String url = "?value=" + salary + "&alignment=" + alignment;

            when(employeeRepository.findBySalaryGreaterThan(salary)).thenReturn(List.of(EMPLOYEE_3, EMPLOYEE_5));

            final ResponseEntity<List<Employee>> result = getEmployeesSalaryResponse(url);

            verify(employeeRepository, times(1)).findBySalaryGreaterThan(salary);
            assertThat(result.getStatusCodeValue(), is(200));
            assertThat(result.getBody(), is(List.of(EMPLOYEE_3, EMPLOYEE_5)));
        }

        @Test
        @DisplayName("with empty list because no employees found with above specified salary")
        void empty_list_above_specified_salary() throws MalformedURLException, URISyntaxException {
            final double salary = 7000.00;
            final String alignment = "above";
            final String url = "?value=" + salary + "&alignment=" + alignment;

            when(employeeRepository.findBySalaryGreaterThan(salary)).thenReturn(Collections.emptyList());

            final ResponseEntity<List<Employee>> result = getEmployeesSalaryResponse(url);

            verify(employeeRepository, times(1)).findBySalaryGreaterThan(salary);
            assertThat(result.getStatusCodeValue(), is(200));
            assertThat(result.getBody(), is(Collections.emptyList()));
        }

        private ResponseEntity<List<Employee>> getEmployeesSalaryResponse(
                final String queryString) throws URISyntaxException, MalformedURLException {
            final URI uri = getUri(SALARY_ENDPOINT, RANDOM_SERVICE_PORT);

            return testRestTemplate.exchange(uri.toURL() + queryString,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {}); //todo: how does it work?
        }
    }
}
