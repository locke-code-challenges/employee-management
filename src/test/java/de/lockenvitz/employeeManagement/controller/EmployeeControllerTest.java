package de.lockenvitz.employeeManagement.controller;

import de.lockenvitz.employeeManagement.base.BaseTest;
import de.lockenvitz.employeeManagement.model.Employee;
import de.lockenvitz.employeeManagement.service.EmployeeServiceInterface;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EmployeeControllerTest extends BaseTest {

    @InjectMocks
    private EmployeeController classUnderTest;

    @Mock
    private EmployeeServiceInterface employeeService;

    @Test
    @DisplayName("createEmployee should create new employee")
    void createEmployee_should_create_new_employee() {
        when(employeeService.save(EMPLOYEE)).thenReturn(EMPLOYEE_2);

        ResponseEntity<Employee> result = classUnderTest.createEmployee(EMPLOYEE);

        assertThat(result.getStatusCodeValue(), is(200));
        assertThat(result.getBody(), is(EMPLOYEE_2));
    }

    @Test
    @DisplayName("getEmployee should return single employee")
    void getEmployee_should_return_single_employee() {
        when(employeeService.findById(ID)).thenReturn(Optional.of(EMPLOYEE_2));

        ResponseEntity<Employee> result = classUnderTest.getEmployee(ID);

        assertThat(result.getStatusCodeValue(), is(200));
        assertThat(result.getBody(), is(EMPLOYEE_2));
    }

    @Test
    @DisplayName("getEmployee should return no employee if no one exists")
    void getEmployee_should_return_no_employee_if_no_one_exists() {
        when(employeeService.findById(ID)).thenReturn(Optional.empty());

        ResponseEntity<Employee> result = classUnderTest.getEmployee(ID);

        assertThat(result.getStatusCodeValue(), is(400));
    }

    @Test
    @DisplayName("getEmployeeByEmployeeId should return single employee")
    void getEmployeeByEmployeeId_should_return_single_employee() {
        when(employeeService.findByEmployeeId(EMPLOYEE_ID)).thenReturn(Optional.of(EMPLOYEE_2));

        ResponseEntity<Employee> result = classUnderTest.getEmployeeByEmployeeId(EMPLOYEE_ID);

        assertThat(result.getStatusCodeValue(), is(200));
        assertThat(result.getBody(), is(EMPLOYEE_2));
    }

    @Test
    @DisplayName("getEmployeeByEmployeeId should return no employee if no one exists")
    void getEmployeeByEmployeeId_should_return_no_employee_if_no_one_exists() {
        when(employeeService.findByEmployeeId(EMPLOYEE_ID)).thenReturn(Optional.empty());

        ResponseEntity<Employee> result = classUnderTest.getEmployeeByEmployeeId(EMPLOYEE_ID);

        assertThat(result.getStatusCodeValue(), is(400));
    }

    @Test
    @DisplayName("updateEmployee should update an employee")
    void updateEmployee_should_update_an_employee() {
        when(employeeService.findById(ID)).thenReturn(Optional.of(EMPLOYEE_2));
        when(employeeService.save(EMPLOYEE_2, EMPLOYEE_5)).thenReturn(EMPLOYEE_5);

        ResponseEntity<Employee> result = classUnderTest.updateEmployee(ID, EMPLOYEE_5);

        assertThat(result.getStatusCodeValue(), is(200));
        assertThat(result.getBody(), is(EMPLOYEE_5));
    }

    @Test
    @DisplayName("updateEmployee should update no employee if no one exists")
    void updateEmployee_should_update_no_employee_if_no_one_exists() {
        //todo: same procedure like the other tests with bad result
    }

    @Test
    @DisplayName("deleteEmployee should delete employee")
    void deleteEmployee_should_delete_employee() {
        //todo: same procedure like the other tests with result ok
    }

    @Test
    @DisplayName("deleteEmployee should delete no employee if no one exists")
    void deleteEmployee_should_delete_no_employee_if_no_one_exists() {
        //todo: same procedure like the other tests with bad result
    }
}