# Employee Management Service

REST Service with CRUD operation to manage employee data

## Features
* REST Interface with CRUD operations
* REST Interface with different HTTP methods to get information about employees 
* process/map JSON data in services
* save data in static DB

## Challenge

**Java Backend Service (Angestelltenverwaltung)**

Bitte erstelle ein Programm zum Speichern und Verwalten von Angestelltendaten.
Nachdem das Programm gestartet wurde, sollen über eine REST-Schnittstelle die vier CRUD-Operationen auf einem Datensatz 
möglich sein. Zusätzlich soll es die Möglichkeit geben die folgenden Informationen abzurufen:

* Liste aller Angestellten
* Liste aller Angestellten mit einem frei wählbaren Mindestverdienst
* Einzelne Angestellte durch Angabe einer Angestelltennummer
* Liste aller männlichen Angestellten
* Liste aller Angestellten mit einem frei wählbaren Nachnamen

Ein Datensatz soll die folgenden Felder enthalten:

* Anrede (Herr / Frau / Divers)
* Vorname
* Nachname
* Verdienst (Inklusive Nachkommastellen)
* Angestelltennummer

Nach einem Neustart der Anwendung sollen die bereits eingegebenen Daten wieder verfügbar sein.

## Requirements for Linux user 

### MySQL Server

`apt update && apt install mysql-server`

### Setup
* Clone the application:

`git clone git@gitlab.com:locke-code-challenges/employee-management.git`

* Login into MySQL Client and create Mysql database

$ `mysql -u root -p`

mysql> `CREATE DATABASE employee;`

* Change mysql username and password as per your installation

open src/main/resources/application.properties

change `spring.datasource.username` and `spring.datasource.password` of your sql database

* Build and run the app using maven

$ `mvn clean install spring-boot:run`

## Explore the REST API

The service is reachable at `http://localhost:8080`.

# API documentation at:

The OpenAPI description and Swagger UI is accessible at the following links and offers the following endpoints 

**Openapi**

`http://localhost:8080/api-docs/`

**Swagger UI**

`http://localhost:8080/swagger-ui.html`

## Some interesting hints to expand the application 

**Validation with Spring Boot validation**

* https://blog.codecentric.de/2017/11/dynamische-validierung-mit-spring-boot-validation/
* https://www.baeldung.com/javax-validation